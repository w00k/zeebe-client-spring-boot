package zeebeclientspringboot.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.zeebe.client.api.response.Topology;
import io.zeebe.client.api.response.WorkflowInstanceResult;
import io.zeebe.spring.client.EnableZeebeClient;
import io.zeebe.spring.client.ZeebeClientLifecycle;
import zeebeclientspringboot.domain.Message;

@RestController
@EnableZeebeClient
public class ZeebeClient {

	@Autowired
	private ZeebeClientLifecycle client;

	@GetMapping("/status")
	public String getStatus() {
		Topology topology = client.newTopologyRequest().send().join();
        return topology.toString();
	}
	
	@GetMapping("/hello-world-client/{name}")
	public Message helloWorldClient(@PathVariable String name) {
		String bpmnProcessId = "Process_0cwblrf";
		
		Message message = new Message();
		Map<String, String> variables = new HashMap<String, String>(); 
		Map<String, Object> variablesResult;
		
		variables.put("name", name);
		
		WorkflowInstanceResult workflowInstanceResult = client.newCreateInstanceCommand().bpmnProcessId(bpmnProcessId).latestVersion().variables(variables).withResult().send().join(); 
		variablesResult = workflowInstanceResult.getVariablesAsMap();
		
		System.out.println("variablesResult name : " + variablesResult.get("name"));
		System.out.println("variablesResult message : " + variablesResult.get("message"));
		System.out.println("variablesResult : " + variablesResult);
		
		message.setMessage(variablesResult.get("message").toString());
		
		return message;
	}

}
