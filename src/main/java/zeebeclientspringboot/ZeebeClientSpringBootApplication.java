package zeebeclientspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.zeebe.spring.client.annotation.ZeebeDeployment;

@SpringBootApplication
@ZeebeDeployment(classPathResources = {"hello-world-process.bpmn"})
public class ZeebeClientSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZeebeClientSpringBootApplication.class, args);
	}

}
